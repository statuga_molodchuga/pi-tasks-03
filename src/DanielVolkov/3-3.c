#include <stdio.h>

#define N 256

int main()
{
	char str[N];
	int Sum[256] = { 0 }, i = 0;

	fgets(str, N, stdin);

	while (str[i] != '\n')
	{
		Sum[(int)str[i]]++;
		i++;
	}

	for (i = 0; i < 256; i++)
	{
		if (Sum[i] != 0)
			printf(" %c - %d ", i, Sum[i]);
	}
	printf("\n");

	return 0;
}